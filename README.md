# ShopSimulator

## Prérequis

Visual Studio 2019<br>
C# 8.0

## Journal de bord

### 05.10.20020

Création en temps réel du diagramme de classe et implémentation de la logique de base

### 12.10.2020

Début des animations graphiques

## Semaine du 19.10.2020

Ajout d'informations sur l'affichage (nombre de clients, nombre de caisses ouvertes, etc...), configuration du nombre de clients et de la vitesse de simulation

### 26.10.2020

Documentation et normalisation