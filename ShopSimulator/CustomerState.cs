﻿// <copyright file="CustomerState.cs" company="CFPT-I">
// Copyright (c) 2020 Florian Burgener
// </copyright>

namespace ShopSimulator
{
    /// <summary>
    /// Represents the customer states.
    /// </summary>
    public enum CustomerState
    {
        /// <summary>
        /// Follows the path to the cash register.
        /// </summary>
        FollowingPath,

        /// <summary>
        /// Waits for a cash register.
        /// </summary>
        WalkingAlongWaitingPath,

        /// <summary>
        /// Walks to the a cash register.
        /// </summary>
        WalkingToCashRegister,

        /// <summary>
        /// Waits at a cash register.
        /// </summary>
        WaitingAtCashRegister,
    }
}
