﻿// <copyright file="CashRegisterState.cs" company="CFPT-I">
// Copyright (c) 2020 Florian Burgener
// </copyright>

namespace ShopSimulator
{
    /// <summary>
    /// Represents the cash register states.
    /// </summary>
    public enum CashRegisterState
    {
        /// <summary>
        /// The cash register is open.
        /// </summary>
        Open,

        /// <summary>
        /// The cash register is close.
        /// </summary>
        Close,
    }
}
