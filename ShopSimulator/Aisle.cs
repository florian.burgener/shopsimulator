﻿// <copyright file="Aisle.cs" company="CFPT-I">
// Copyright (c) 2020 Florian Burgener
// </copyright>

namespace ShopSimulator
{
    using System;

    /// <summary>
    /// Represents an aisle.
    /// </summary>
    public class Aisle : ICloneable
    {
        /// <summary>
        /// Gets or sets the direction of the aisle.
        /// </summary>
        public Direction Direction { get; set; }

        /// <summary>
        /// Gets or sets the distance of the aisle.
        /// </summary>
        public float Distance { get; set; }

        /// <summary>
        /// Clones the aisle.
        /// </summary>
        /// <returns>A clone of the aisle.</returns>
        public object Clone()
        {
            return new Aisle()
            {
                Direction = this.Direction,
                Distance = this.Distance,
            };
        }
    }
}
