﻿// <copyright file="CashRegisterManager.cs" company="CFPT-I">
// Copyright (c) 2020 Florian Burgener
// </copyright>

namespace ShopSimulator
{
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;

    /// <summary>
    /// Represents all the cash registers.
    /// </summary>
    public class CashRegisterManager
    {
        private const int LengthBetweenCashRegisters = 3;
        private static readonly SolidBrush WaitngPathColor = new SolidBrush(Color.FromArgb(151, 132, 102));
        private readonly List<CashRegister> cashRegisters = new List<CashRegister>();

        /// <summary>
        /// Initializes a new instance of the <see cref="CashRegisterManager"/> class.
        /// </summary>
        /// <param name="cashRegisterCount">The number of cash registers.</param>
        public CashRegisterManager(int cashRegisterCount)
        {
            for (int i = 0; i < cashRegisterCount; i++)
            {
                this.cashRegisters.Add(new CashRegister());
            }
        }

        /// <summary>
        /// Gets the position x of the most left cash register.
        /// </summary>
        public float X { get; private set; }

        /// <summary>
        /// Gets the width of the waiting path.
        /// </summary>
        public float WaitingPathWidth => (ShopMap.PathWidth * (this.cashRegisters.Count - 1) * LengthBetweenCashRegisters) + ShopMap.PathWidth;

        /// <summary>
        /// Gets the count of cash registers.
        /// </summary>
        public int Count => this.cashRegisters.Count;

        /// <summary>
        /// Gets the count of opened cash registers.
        /// </summary>
        public int CountOpen => this.cashRegisters.Where(c => c.IsOpen()).Count();

        /// <summary>
        /// Gets the total of available places.
        /// </summary>
        public int AvailablePlace => this.cashRegisters.Where(c => c.IsOpen()).Select(c => c.AvailablePlace).Sum();

        /// <summary>
        /// Updates the cash registers.
        /// </summary>
        public void Update()
        {
            this.cashRegisters.ForEach(cashRegister => cashRegister.Update());
        }

        /// <summary>
        /// Draw the cash registers.
        /// </summary>
        /// <param name="e">The PaintEventArgs.</param>
        /// <param name="x">The x origin.</param>
        /// <param name="y">The y origin.</param>
        public void Draw(PaintEventArgs e, float x, float y)
        {
            this.X = x - this.WaitingPathWidth + ShopMap.PathWidth;
            e.Graphics.FillRectangle(WaitngPathColor, this.X, y, this.WaitingPathWidth, ShopMap.PathWidth);

            foreach (var cashRegister in this.cashRegisters)
            {
                cashRegister.Draw(e, x, y + ShopMap.PathWidth);

                x -= ShopMap.PathWidth * LengthBetweenCashRegisters;
            }
        }

        /// <summary>
        /// Checks if the customer has reached the edge of the waiting path.
        /// </summary>
        /// <param name="x">The x position of the customer.</param>
        /// <returns>True if the customer has reached the edge of the waiting path.</returns>
        public bool HasReachedWaitingPathEdge(float x)
        {
            return x <= this.X || x + Customer.Width >= this.X + this.WaitingPathWidth;
        }

        /// <summary>
        /// Gets the closest cash register that is available (open).
        /// </summary>
        /// <returns>The closest cash register available.</returns>
        public CashRegister GetClosestCashRegisterAvailable()
        {
            foreach (var cashRegister in this.cashRegisters)
            {
                if (cashRegister.IsOpen() && !cashRegister.IsFull())
                {
                    return cashRegister;
                }
            }

            return null;
        }

        /// <summary>
        /// Opens a cash register.
        /// </summary>
        public void OpenCashRegister()
        {
            CashRegister cashRegister = this.cashRegisters.Where(cashRegister => !cashRegister.IsOpen() && !cashRegister.IsFull()).FirstOrDefault();

            if (cashRegister == null)
            {
                return;
            }

            cashRegister.Open();
        }
    }
}
