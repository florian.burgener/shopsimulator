﻿// <copyright file="Shop.cs" company="CFPT-I">
// Copyright (c) 2020 Florian Burgener
// </copyright>

namespace ShopSimulator
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows.Forms;

    /// <summary>
    /// Represents the shop.
    /// </summary>
    public class Shop
    {
        private const int CashRegisterCount = 9;
        private const int CustomerWaitingTimeBeforeCashRegisterOpen = 30000;
        private const int DefaultCustomerLimit = 10;
        private readonly List<Customer> customers = new List<Customer>();
        private readonly Stopwatch cashRegisterStopwatch = new Stopwatch();
        private int customerLimit = DefaultCustomerLimit;

        /// <summary>
        /// Initializes a new instance of the <see cref="Shop"/> class.
        /// </summary>
        public Shop()
        {
            this.InitMap();
            this.cashRegisterStopwatch.Start();
        }

        /// <summary>
        /// Gets or sets the name of the shop.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the customer count in the shop.
        /// </summary>
        public int CustomerCount => this.customers.Count;

        /// <summary>
        /// Gets or sets the customer limit in the shop.
        /// </summary>
        public int CustomerLimit
        {
            get
            {
                return this.customerLimit;
            }

            set
            {
                this.customerLimit = Math.Max(0, value);
            }
        }

        /// <summary>
        /// Gets the shop map.
        /// </summary>
        public ShopMap ShopMap { get; } = new ShopMap();

        /// <summary>
        /// Gets the cash register manager.
        /// </summary>
        public CashRegisterManager CashRegisters { get; } = new CashRegisterManager(CashRegisterCount);

        /// <summary>
        /// Gets the number of customers waiting for a cash register.
        /// </summary>
        public int CustomerWaitingForCashRegisterCount => this.customers.Where(c => c.IsWaitingForCashRegister()).Count();

        /// <summary>
        /// Adds a customer to the shop.
        /// </summary>
        /// <param name="customer">The new customer.</param>
        public void AddCustomer(Customer customer)
        {
            this.customers.Add(customer);
        }

        /// <summary>
        /// Removes a customer from the shop.
        /// </summary>
        /// <param name="customer">The customer.</param>
        public void RemoveCustomer(Customer customer)
        {
            this.customers.Remove(customer);
        }

        /// <summary>
        /// Updates the shop.
        /// </summary>
        public void Update()
        {
            bool shouldOpenCashRegister = false;

            this.CashRegisters.Update();
            this.customers.ForEach(customer =>
            {
                customer.Update();

                if (customer.WaitingTime > CustomerWaitingTimeBeforeCashRegisterOpen / Simulator.SimulationSpeed)
                {
                    shouldOpenCashRegister = true;
                }
            });

            if (shouldOpenCashRegister && this.cashRegisterStopwatch.ElapsedMilliseconds > 1000)
            {
                this.cashRegisterStopwatch.Restart();
                this.customers.ForEach(customer => customer.ResetWaitingTime());
                this.CashRegisters.OpenCashRegister();
            }
        }

        /// <summary>
        /// Draw the shop.
        /// </summary>
        /// <param name="e">The PaintEventArgs.</param>
        public void Draw(PaintEventArgs e)
        {
            var (x, y) = this.ShopMap.Draw(e);
            this.CashRegisters.Draw(e, x, y);
            this.customers.ForEach(customer => customer.Draw(e));
        }

        /// <summary>
        /// Checks if the shop is full.
        /// </summary>
        /// <returns>True if the shop is full.</returns>
        public bool IsFull()
        {
            return this.CustomerCount >= this.CustomerLimit;
        }

        /// <summary>
        /// Gets the time before a cash register will open in milliseconds.
        /// </summary>
        /// <returns>Time before a cash register open in milliseconds.</returns>
        public long GetTimeBeforeCashRegisterOpen()
        {
            int biggestWaitingTime = this.customers.Select(c => c.WaitingTime).DefaultIfEmpty().Max();
            float time = (CustomerWaitingTimeBeforeCashRegisterOpen / Simulator.SimulationSpeed) - biggestWaitingTime;
            time *= Simulator.SimulationSpeed;

            return (long)time;
        }

        /// <summary>
        /// Init the shop map.
        /// </summary>
        private void InitMap()
        {
            int width = ShopMap.PathWidth;

            this.ShopMap.AddAisle(Direction.Down, width * 16);
            this.ShopMap.AddAisle(Direction.Right, width * 20);
            this.ShopMap.AddAisle(Direction.Up, width * 10);
            this.ShopMap.AddAisle(Direction.Right, width * 2);
            this.ShopMap.AddAisle(Direction.Down, width * 10);
            this.ShopMap.AddAisle(Direction.Right, width * 2);
            this.ShopMap.AddAisle(Direction.Up, width * 12);
            this.ShopMap.AddAisle(Direction.Left, width * 6);
            this.ShopMap.AddAisle(Direction.Down, width * 10);
            this.ShopMap.AddAisle(Direction.Left, width * 16);
            this.ShopMap.AddAisle(Direction.Up, width * 14);
            this.ShopMap.AddAisle(Direction.Right, width * 2);
            this.ShopMap.AddAisle(Direction.Down, width * 12);
            this.ShopMap.AddAisle(Direction.Right, width * 12);
            this.ShopMap.AddAisle(Direction.Up, width * 10);
            this.ShopMap.AddAisle(Direction.Left, width * 2);
            this.ShopMap.AddAisle(Direction.Down, width * 8);
            this.ShopMap.AddAisle(Direction.Left, width * 8);
            this.ShopMap.AddAisle(Direction.Up, width * 2);
            this.ShopMap.AddAisle(Direction.Right, width * 6);
            this.ShopMap.AddAisle(Direction.Up, width * 2);
            this.ShopMap.AddAisle(Direction.Left, width * 6);
            this.ShopMap.AddAisle(Direction.Up, width * 2);
            this.ShopMap.AddAisle(Direction.Right, width * 6);
            this.ShopMap.AddAisle(Direction.Up, width * 2);
            this.ShopMap.AddAisle(Direction.Left, width * 6);
            this.ShopMap.AddAisle(Direction.Up, width * 2);
            this.ShopMap.AddAisle(Direction.Right, width * 12);
            this.ShopMap.AddAisle(Direction.Down, width * 2);
            this.ShopMap.AddAisle(Direction.Right, width * 2);
            this.ShopMap.AddAisle(Direction.Up, width * 2);
            this.ShopMap.AddAisle(Direction.Right, width * 2);
            this.ShopMap.AddAisle(Direction.Down, width * 2);
            this.ShopMap.AddAisle(Direction.Right, width * 2);
            this.ShopMap.AddAisle(Direction.Up, width * 2);
            this.ShopMap.AddAisle(Direction.Right, width * 2);
            this.ShopMap.AddAisle(Direction.Down, width * 18);
        }
    }
}
