﻿// <copyright file="ShopMap.cs" company="CFPT-I">
// Copyright (c) 2020 Florian Burgener
// </copyright>

namespace ShopSimulator
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;

    /// <summary>
    /// Represents the map of a shop.
    /// </summary>
    public class ShopMap : ICloneable
    {
        /// <summary>
        /// The length of the path.
        /// </summary>
        public const int PathWidth = 30;

        /// <summary>
        /// Gets or sets the shop aisles.
        /// </summary>
        public List<Aisle> Aisles { get; set; } = new List<Aisle>();

        /// <summary>
        /// Adds a aisle to the shop.
        /// </summary>
        /// <param name="direction">The direction of the aisle.</param>
        /// <param name="distance">The distance of the aisle.</param>
        public void AddAisle(Direction direction, int distance)
        {
            this.Aisles.Add(new Aisle
            {
                Direction = direction,
                Distance = distance,
            });
        }

        /// <summary>
        /// Draw the aisles.
        /// </summary>
        /// <param name="e">The PaintEventArgs.</param>
        /// <returns>The last position of the aisle.</returns>
        public (float X, float Y) Draw(PaintEventArgs e)
        {
            float x = 0;
            float y = 0;

            Brush color = new SolidBrush(Color.FromArgb(255, 218, 26));
            int width = PathWidth;

            foreach (Aisle aisle in this.Aisles)
            {
                switch (aisle.Direction)
                {
                    case Direction.Right:
                        e.Graphics.FillRectangle(color, x, y, aisle.Distance, width);
                        x += aisle.Distance;
                        break;
                    case Direction.Up:
                        e.Graphics.FillRectangle(color, x, y - aisle.Distance + width, width, aisle.Distance);
                        y -= aisle.Distance;
                        break;
                    case Direction.Left:
                        e.Graphics.FillRectangle(color, x - aisle.Distance, y, aisle.Distance + width, width);
                        x -= aisle.Distance;
                        break;
                    case Direction.Down:
                        e.Graphics.FillRectangle(color, x, y, width, aisle.Distance);
                        y += aisle.Distance;
                        break;
                }
            }

            return (x, y);
        }

        /// <summary>
        /// Clones the map.
        /// </summary>
        /// <returns>The cloned map.</returns>
        public object Clone()
        {
            return new ShopMap()
            {
                Aisles = this.Aisles.Select(aisle => (Aisle)aisle.Clone()).ToList(),
            };
        }
    }
}
