﻿// <copyright file="CashRegister.cs" company="CFPT-I">
// Copyright (c) 2020 Florian Burgener
// </copyright>

namespace ShopSimulator
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Drawing;
    using System.Globalization;
    using System.Linq;
    using System.Windows.Forms;

    /// <summary>
    /// Represents a cash register.
    /// </summary>
    public class CashRegister
    {
        private const int QueueLength = 5;
        private const int TimeBeforeClose = 30000;
        private const int TextMarginTop = 4;
        private static readonly SolidBrush QueueColor = new SolidBrush(Color.FromArgb(151, 132, 102));
        private static readonly Brush OpenColor = new SolidBrush(Color.FromArgb(46, 204, 113));
        private static readonly Brush CloseColor = Brushes.Red;
        private readonly Stopwatch customerStopwatch = new Stopwatch();
        private readonly Stopwatch closeStopwatch = new Stopwatch();
        private CashRegisterState state = CashRegisterState.Close;

        /// <summary>
        /// Gets the position x.
        /// </summary>
        public float X { get; private set; }

        /// <summary>
        /// Gets the position y.
        /// </summary>
        public float Y { get; private set; }

        /// <summary>
        /// Gets the customers list.
        /// </summary>
        public List<Customer> Customers { get; } = new List<Customer>();

        /// <summary>
        /// Gets the number of available place.
        /// </summary>
        public int AvailablePlace => QueueLength - this.Customers.Count;

        /// <summary>
        /// Updates the cash register.
        /// </summary>
        public void Update()
        {
            this.CheckCustomers();
            this.CheckClose();
        }

        /// <summary>
        /// Draw the cash register.
        /// </summary>
        /// <param name="e">The PaintEventArgs.</param>
        /// <param name="x">The top left x origin.</param>
        /// <param name="y">The top left y origin.</param>
        public void Draw(PaintEventArgs e, float x, float y)
        {
            this.X = x;
            this.Y = y;

            e.Graphics.FillRectangle(QueueColor, x, y, ShopMap.PathWidth, ShopMap.PathWidth * (QueueLength - 1));
            e.Graphics.FillRectangle(this.GetColor(), x, y + (ShopMap.PathWidth * (QueueLength - 1)), ShopMap.PathWidth, ShopMap.PathWidth);

            if (!this.Customers.Any())
            {
                return;
            }

            double elapsedTime = Math.Round(((this.Customers[0].CashOutTime / Simulator.SimulationSpeed) - this.customerStopwatch.ElapsedMilliseconds) / 1000.0 * Simulator.SimulationSpeed, 2);
            string elapsedTimeFormatted = elapsedTime.ToString("0.00", new CultureInfo("en-US"));
            e.Graphics.DrawString(elapsedTimeFormatted, SystemFonts.DefaultFont, Brushes.White, x, y + (ShopMap.PathWidth * QueueLength) + TextMarginTop);
        }

        /// <summary>
        /// Opens the cash register.
        /// </summary>
        public void Open()
        {
            this.state = CashRegisterState.Open;
            this.closeStopwatch.Restart();
        }

        /// <summary>
        /// Checks if the cash register is opened.
        /// </summary>
        /// <returns>True if the cash register is open.</returns>
        public bool IsOpen() => this.state == CashRegisterState.Open;

        /// <summary>
        /// Closes the cash register.
        /// </summary>
        public void Close()
        {
            this.state = CashRegisterState.Close;
            this.closeStopwatch.Stop();
        }

        /// <summary>
        /// Checks if the cash register is full of customers.
        /// </summary>
        /// <returns>True if the cash register is full of customers.</returns>
        public bool IsFull()
        {
            return this.Customers.Count == QueueLength;
        }

        /// <summary>
        /// Gets the position y of a customer.
        /// </summary>
        /// <param name="customer">The targetted customer.</param>
        /// <returns>The position y of the targetted customer.</returns>
        public float GetPositionY(Customer customer)
        {
            int index = this.Customers.FindIndex(c => c.Equals(customer));

            return this.Y + ((QueueLength - index) * ShopMap.PathWidth) - (ShopMap.PathWidth / 2) - (Customer.Width / 2);
        }

        /// <summary>
        /// Adds the customer to the cash register queue.
        /// </summary>
        /// <param name="customer">The targetted customer.</param>
        public void AddCustomer(Customer customer)
        {
            this.Customers.Add(customer);

            if (!this.customerStopwatch.IsRunning)
            {
                this.customerStopwatch.Start();
            }
        }

        /// <summary>
        /// Gets the color according to the cash register state.
        /// </summary>
        /// <returns>The color according to the cash register state.</returns>
        private Brush GetColor() => this.state switch
        {
            CashRegisterState.Open => OpenColor,
            _ => CloseColor,
        };

        /// <summary>
        /// Checks if customers has finish to checkout.
        /// </summary>
        private void CheckCustomers()
        {
            if (this.Customers.Count == 0)
            {
                this.customerStopwatch.Restart();
                return;
            }

            Customer customer = this.Customers[0];

            if (this.customerStopwatch.ElapsedMilliseconds < customer.CashOutTime / Simulator.SimulationSpeed)
            {
                return;
            }

            customer.LeaveShop();
            this.customerStopwatch.Restart();
            this.Customers.RemoveAt(0);

            if (!this.Customers.Any())
            {
                this.customerStopwatch.Stop();
            }
        }

        /// <summary>
        /// Check if the cash register should close.
        /// </summary>
        private void CheckClose()
        {
            if (this.closeStopwatch.ElapsedMilliseconds < TimeBeforeClose / Simulator.SimulationSpeed)
            {
                return;
            }

            if (this.Customers.Count == 0)
            {
                this.Close();
            }
            else
            {
                this.closeStopwatch.Restart();
            }
        }
    }
}
