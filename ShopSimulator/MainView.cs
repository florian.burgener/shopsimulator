﻿// <copyright file="MainView.cs" company="CFPT-I">
// Copyright (c) 2020 Florian Burgener
// </copyright>

namespace ShopSimulator
{
    using System;
    using System.Diagnostics;
    using System.Drawing;
    using System.Globalization;
    using System.Windows.Forms;

    /// <summary>
    /// Represents the main view behavior.
    /// </summary>
    public partial class MainView : Form
    {
        private const int MillisecondsPerSecond = 1000;

        private const int RefreshRate = 120;

        private const int FormWidth = 1124;

        private const int FormHeight = 780;

        private const int ControlMargin = 8;

        private const int ControlAreaWidth = 298 + (ControlMargin * 2);

        private readonly Timer timer = new Timer();
        private readonly Simulator simulator = new Simulator();
        private readonly Stopwatch sp = new Stopwatch();

        /// <summary>
        /// Initializes a new instance of the <see cref="MainView"/> class.
        /// </summary>
        public MainView()
        {
            this.DoubleBuffered = true;
            this.InitializeComponent();

            this.timer.Interval = MillisecondsPerSecond / RefreshRate;
            this.timer.Tick += this.Timer_Tick;
            this.timer.Start();

            this.sp.Start();
        }

        /// <summary>
        /// Redraw the shop on each tick.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The EventArgs.</param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            this.Invalidate();

            this.elapsedTimeLabel.Text = $"T+{this.ConvertMillisecondsToSeconds(this.sp.ElapsedMilliseconds)}";
            this.cashRegistersLabel.Text = $"Caisses ouvertes : {this.simulator.Shop.CashRegisters.CountOpen} / {this.simulator.Shop.CashRegisters.Count}";
            this.availablePlaceLabel.Text = $"Places disponibles : {this.simulator.Shop.CashRegisters.AvailablePlace}";
            this.customersWaitingLabel.Text = $"Clients en attente : {this.simulator.Shop.CustomerWaitingForCashRegisterCount}";
            this.customersLabel.Text = $"Clients : {this.simulator.Shop.CustomerCount} / {this.simulator.Shop.CustomerLimit}";
            this.cashRegisterOpenTimeLabel.Text = $"Temps avant ouverture : {this.ConvertMillisecondsToSeconds(this.simulator.Shop.GetTimeBeforeCashRegisterOpen())}";
            this.simulationSpeedLabel.Text = $"Vitesse de simulation x{Simulator.SimulationSpeed}";
        }

        /// <summary>
        /// Center and aligns elements on load.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The EventArgs.</param>
        private void MainView_Load(object sender, EventArgs e)
        {
            this.ClientSize = new Size(FormWidth, FormHeight);
            this.Text = this.simulator.Shop.Name;
            this.CenterToScreen();

            this.CenterLabel(this.elapsedTimeLabel);
            this.CenterLabel(this.cashRegistersLabel);
            this.CenterLabel(this.customersWaitingLabel);
            this.CenterLabel(this.availablePlaceLabel);
            this.CenterLabel(this.cashRegisterOpenTimeLabel);
            this.CenterLabel(this.customersLabel);
            this.CenterLabel(this.customersControlLabel);
            this.CenterLabel(this.simulationSpeedLabel);
            this.CenterLabel(this.simulationSpeedControlLabel);

            this.CenterButton(this.addCustomerButton, HorizontalAlignment.Left);
            this.CenterButton(this.removeCustomerButton, HorizontalAlignment.Right);
            this.CenterButton(this.increaseSimulationSpeed, HorizontalAlignment.Left);
            this.CenterButton(this.decreaseSimulationSpeed, HorizontalAlignment.Right);
        }

        /// <summary>
        /// Paint the shop.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The PaintEventArgs.</param>
        private void MainView_Paint(object sender, PaintEventArgs e)
        {
            this.simulator.Draw(e);
        }

        private void AddCustomerButton_Click(object sender, EventArgs e)
        {
            this.simulator.Shop.CustomerLimit += 1;
        }

        private void RemoveCustomerButton_Click(object sender, EventArgs e)
        {
            this.simulator.Shop.CustomerLimit -= 1;
        }

        private void IncreaseSimulationSpeed_Click(object sender, EventArgs e)
        {
            Simulator.SimulationSpeed += 1;
        }

        private void DecreaseSimulationSpeed_Click(object sender, EventArgs e)
        {
            Simulator.SimulationSpeed -= 1;
        }

        /// <summary>
        /// Convert milliseconds to displayable string.
        /// </summary>
        /// <param name="milliseconds">The number of milliseconds.</param>
        /// <returns>The formatted string.</returns>
        private string ConvertMillisecondsToSeconds(long milliseconds)
        {
            return Math.Round(milliseconds / 1000.0, 2).ToString("0.00", new CultureInfo("en-US"));
        }

        /// <summary>
        /// Center a label to the right area.
        /// </summary>
        /// <param name="label">The target label.</param>
        private void CenterLabel(Label label)
        {
            label.Left = FormWidth - label.Width - ControlMargin;
        }

        /// <summary>
        /// Center the button to the right area.
        /// </summary>
        /// <param name="button">The target button.</param>
        /// <param name="alignment">The alignement direction.</param>
        private void CenterButton(Button button, HorizontalAlignment alignment)
        {
            button.Left = FormWidth - ((ControlAreaWidth - button.Width) / 2) - button.Width + (((button.Width / 2) + (ControlMargin / 2)) * (alignment == HorizontalAlignment.Left ? -1 : 1));
        }
    }
}
