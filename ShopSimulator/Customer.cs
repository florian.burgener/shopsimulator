﻿// <copyright file="Customer.cs" company="CFPT-I">
// Copyright (c) 2020 Florian Burgener
// </copyright>

namespace ShopSimulator
{
    using System.Diagnostics;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;

    /// <summary>
    /// Represents a customer.
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// Width of a customer.
        /// </summary>
        public const int Width = 12;

        /// <summary>
        /// Height of a customer.
        /// </summary>
        public const int Height = 12;

        private const int SpeedMultiplicator = 50;
        private const int AngryElapsedTime = 10000;
        private const int MinCashOutTime = 15000;
        private const int MaxCashOutTime = 120000;

        private readonly Stopwatch updateStopwatch = new Stopwatch();
        private readonly Stopwatch waitingTimeStopwatch = new Stopwatch();
        private long lastUpdateTime = 0;
        private Shop shop;
        private ShopMap shopMap = null;
        private CashRegister cashRegister = null;
        private Direction direction = Direction.Down;
        private CustomerState state = CustomerState.FollowingPath;

        /// <summary>
        /// Initializes a new instance of the <see cref="Customer"/> class.
        /// </summary>
        /// <param name="age">The age of the customer.</param>
        public Customer(int age)
        {
            this.updateStopwatch.Start();
            this.Age = age;
            this.Speed = this.CalculateSpeed() * SpeedMultiplicator;
            this.X = (ShopMap.PathWidth - Width) / 2;
            this.Y = (ShopMap.PathWidth - Height) / 2;
            this.CashOutTime = RandomNumber.Instance.Next(MinCashOutTime, MaxCashOutTime);
        }

        /// <summary>
        /// Gets the age of the customer.
        /// </summary>
        public int Age { get; }

        /// <summary>
        /// Gets the speed in pixels per second.
        /// </summary>
        public float Speed { get; }

        /// <summary>
        /// Gets the position x.
        /// </summary>
        public float X { get; private set; }

        /// <summary>
        /// Gets the position y.
        /// </summary>
        public float Y { get; private set; }

        /// <summary>
        /// Gets the waiting time.
        /// </summary>
        public int WaitingTime => (int)this.waitingTimeStopwatch.ElapsedMilliseconds;

        /// <summary>
        /// Gets the cash out time.
        /// </summary>
        public int CashOutTime { get; }

        /// <summary>
        /// Bring a customer into a store.
        /// </summary>
        /// <param name="shop">The target shop.</param>
        public void EnterShop(Shop shop)
        {
            this.shop = shop;
            this.shopMap = (ShopMap)shop.ShopMap.Clone();
            shop.AddCustomer(this);
        }

        /// <summary>
        /// Take a customer out of the store.
        /// </summary>
        public void LeaveShop()
        {
            this.shop.RemoveCustomer(this);
        }

        /// <summary>
        /// Updates the customer.
        /// </summary>
        public void Update()
        {
            switch (this.state)
            {
                case CustomerState.FollowingPath:
                    this.FollowPath();
                    break;
                case CustomerState.WalkingAlongWaitingPath:
                    this.WalkAlongWaitingPath();
                    break;
                case CustomerState.WalkingToCashRegister:
                    this.WalkToCashRegister();
                    break;
                case CustomerState.WaitingAtCashRegister:
                    this.WaitAtCashRegister();
                    break;
            }

            this.lastUpdateTime = this.updateStopwatch.ElapsedMilliseconds;
        }

        /// <summary>
        /// Draws the customer.
        /// </summary>
        /// <param name="e">The PaintEventArgs.</param>
        public void Draw(PaintEventArgs e)
        {
            e.Graphics.FillEllipse(this.IsAngry() ? Brushes.Red : Brushes.Black, this.X, this.Y, Width, Height);
        }

        /// <summary>
        /// Checks if the customer is angry.
        /// </summary>
        /// <returns>True if the customer is angry.</returns>
        public bool IsAngry()
        {
            return this.WaitingTime > AngryElapsedTime / Simulator.SimulationSpeed;
        }

        /// <summary>
        /// Resets the waiting time by restarting the timer.
        /// </summary>
        public void ResetWaitingTime()
        {
            if (this.state == CustomerState.WalkingAlongWaitingPath || this.state == CustomerState.WalkingToCashRegister)
            {
                this.waitingTimeStopwatch.Restart();
            }
        }

        /// <summary>
        /// Checks if the customer is waiting for a cash register.
        /// </summary>
        /// <returns>True if the customer is waiting for a cash register.</returns>
        public bool IsWaitingForCashRegister()
        {
            return this.state == CustomerState.WalkingAlongWaitingPath;
        }

        private void FollowPath()
        {
            float speed = this.GetSpeed();
            Aisle aisle = this.shopMap.Aisles[0];

            if (aisle.Distance - speed <= 0)
            {
                speed = aisle.Distance;
                this.shopMap.Aisles.RemoveAt(0);
            }
            else
            {
                aisle.Distance -= speed;
            }

            switch (aisle.Direction)
            {
                case Direction.Right:
                    this.X += speed;
                    break;
                case Direction.Up:
                    this.Y -= speed;
                    break;
                case Direction.Left:
                    this.X -= speed;
                    break;
                case Direction.Down:
                    this.Y += speed;
                    break;
            }

            if (!this.shopMap.Aisles.Any())
            {
                this.shopMap = null;
                this.direction = Direction.Left;
                this.waitingTimeStopwatch.Start();
                this.CheckAvailableCashRegister();
            }
        }

        /// <summary>
        /// Moves customer along waiting path.
        /// </summary>
        private void WalkAlongWaitingPath()
        {
            switch (this.direction)
            {
                case Direction.Right:
                    this.X += this.GetSpeed();
                    break;
                case Direction.Left:
                    this.X -= this.GetSpeed();
                    break;
            }

            if (this.shop.CashRegisters.HasReachedWaitingPathEdge(this.X))
            {
                this.ReverseDirection();
                this.FixPositionAlongWaitingPath();
            }

            this.CheckAvailableCashRegister();
        }

        /// <summary>
        /// Moves customer to the cash register.
        /// </summary>
        private void WalkToCashRegister()
        {
            float cashRegisterX = this.cashRegister.X + (ShopMap.PathWidth / 2) - (Width / 2);

            if (this.X > cashRegisterX)
            {
                this.direction = Direction.Left;
                this.X -= this.GetSpeed();
            }
            else
            {
                this.direction = Direction.Right;
                this.X += this.GetSpeed();
            }

            if (!this.HasReachedCashRegister())
            {
                return;
            }

            if (this.cashRegister.IsFull())
            {
                this.state = CustomerState.WalkingAlongWaitingPath;
                return;
            }

            this.cashRegister.AddCustomer(this);
            this.waitingTimeStopwatch.Stop();
            this.waitingTimeStopwatch.Reset();
            this.X = cashRegisterX;
            this.state = CustomerState.WaitingAtCashRegister;
        }

        /// <summary>
        /// Move the customer on the queue.
        /// </summary>
        private void WaitAtCashRegister()
        {
            float y = this.cashRegister.GetPositionY(this);

            if (this.Y < y)
            {
                this.Y += this.GetSpeed();
            }
            else
            {
                this.Y = y;
            }
        }

        /// <summary>
        /// Calculates the speed in function of the age.
        /// </summary>
        /// <returns>The speed.</returns>
        private float CalculateSpeed()
        {
            if (this.Age >= 18 && this.Age < 25)
            {
                return 4;
            }

            if (this.Age >= 25 && this.Age < 35)
            {
                return 3.5F;
            }

            if (this.Age >= 35 && this.Age < 60)
            {
                return 3;
            }

            if (this.Age >= 60 && this.Age < 80)
            {
                return 2;
            }

            return 1;
        }

        /// <summary>
        /// Get the speed scaled to the elapsed time.
        /// </summary>
        /// <returns>The speed scaled.</returns>
        private float GetSpeed()
        {
            return this.Speed * (this.updateStopwatch.ElapsedMilliseconds - this.lastUpdateTime) / 1000 * Simulator.SimulationSpeed;
        }

        /// <summary>
        /// Reverse the direction.
        /// </summary>
        private void ReverseDirection()
        {
            switch (this.direction)
            {
                case Direction.Right:
                    this.direction = Direction.Left;
                    break;
                case Direction.Left:
                    this.direction = Direction.Right;
                    break;
            }
        }

        /// <summary>
        /// Sets the position to the edge.
        /// </summary>
        private void FixPositionAlongWaitingPath()
        {
            switch (this.direction)
            {
                case Direction.Right:
                    this.X = this.shop.CashRegisters.X;
                    break;
                case Direction.Left:
                    this.X = this.shop.CashRegisters.X + this.shop.CashRegisters.WaitingPathWidth - Width;
                    break;
            }
        }

        /// <summary>
        /// Checks if there is available cash register.
        /// </summary>
        private void CheckAvailableCashRegister()
        {
            this.state = CustomerState.WalkingToCashRegister;
            this.cashRegister = this.shop.CashRegisters.GetClosestCashRegisterAvailable();

            if (this.cashRegister == null)
            {
                this.state = CustomerState.WalkingAlongWaitingPath;
            }
            else
            {
                this.state = CustomerState.WalkingToCashRegister;
            }
        }

        /// <summary>
        /// Checks if the customer has reached the cash register.
        /// </summary>
        /// <returns>True if the customer has reached the cash register.</returns>
        private bool HasReachedCashRegister()
        {
            if (this.direction == Direction.Left)
            {
                return this.X + (Width / 2) <= this.cashRegister.X + (ShopMap.PathWidth / 2);
            }

            if (this.direction == Direction.Right)
            {
                return this.X + (Width / 2) >= this.cashRegister.X + (ShopMap.PathWidth / 2);
            }

            return false;
        }
    }
}
