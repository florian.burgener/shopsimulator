﻿// <copyright file="RandomNumber.cs" company="CFPT-I">
// Copyright (c) 2020 Florian Burgener
// </copyright>

namespace ShopSimulator
{
    using System;

    /// <summary>
    /// Represents a singleton arround a Random.
    /// </summary>
    public sealed class RandomNumber : Random
    {
        private static readonly object Padlock = new object();

        private static RandomNumber instance = null;

        /// <summary>
        /// Gets the RandomNumber isntance.
        /// </summary>
        public static RandomNumber Instance
        {
            get
            {
                lock (Padlock)
                {
                    if (instance == null)
                    {
                        instance = new RandomNumber();
                    }

                    return instance;
                }
            }
        }

        /// <summary>
        /// Generates a random number.
        /// </summary>
        /// <param name="minValue">The min value (included).</param>
        /// <param name="maxValue">The max value (excluded).</param>
        /// <returns>A random number between the range.</returns>
        public static int Generate(int minValue, int maxValue)
        {
            return Instance.Next(minValue, maxValue);
        }
    }
}
