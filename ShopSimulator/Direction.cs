﻿// <copyright file="Direction.cs" company="CFPT-I">
// Copyright (c) 2020 Florian Burgener
// </copyright>

namespace ShopSimulator
{
    /// <summary>
    /// Represents basic directions.
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// Right.
        /// </summary>
        Right,

        /// <summary>
        /// Up.
        /// </summary>
        Up,

        /// <summary>
        /// Left.
        /// </summary>
        Left,

        /// <summary>
        /// Down.
        /// </summary>
        Down,
    }
}
