﻿// <copyright file="Simulator.cs" company="CFPT-I">
// Copyright (c) 2020 Florian Burgener
// </copyright>

namespace ShopSimulator
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Windows.Forms;

    /// <summary>
    /// Represents the simulator.
    /// </summary>
    public class Simulator
    {
        /// <summary>
        /// The number of milliseconds per second.
        /// </summary>
        public const int MillisecondsPerSecond = 1000;

        /// <summary>
        /// The refresh rate.
        /// </summary>
        public const int RefreshRate = 120;

        /// <summary>
        /// The interval between customer creation.
        /// </summary>
        public const int CreateCustomerInterval = 3000;

        /// <summary>
        /// The default simulation speed.
        /// </summary>
        public const int DefaultSimulationSpeed = 4;

        private const int MinimumSimulationSpeed = 1;

        private static float simulationSpeed = DefaultSimulationSpeed;
        private readonly Timer timer = new Timer();
        private readonly Stopwatch customerTraffic = new Stopwatch();

        /// <summary>
        /// Initializes a new instance of the <see cref="Simulator"/> class.
        /// </summary>
        public Simulator()
        {
            this.InitTimer();
            this.Shop.Name = "IKEA";
            this.CreateCustomer();
            this.customerTraffic.Start();
        }

        /// <summary>
        /// Gets or sets the simulation speed.
        /// </summary>
        public static float SimulationSpeed
        {
            get
            {
                return simulationSpeed;
            }

            set
            {
                simulationSpeed = Math.Max(MinimumSimulationSpeed, value);
            }
        }

        /// <summary>
        /// Gets the shop.
        /// </summary>
        public Shop Shop { get; } = new Shop();

        /// <summary>
        /// Gets the update interval.
        /// </summary>
        /// <returns>The update interval.</returns>
        public static int GetUpdateInterval()
        {
            return (int)((float)MillisecondsPerSecond / RefreshRate);
        }

        /// <summary>
        /// Draw the shop.
        /// </summary>
        /// <param name="e">The PaintEventArgs.</param>
        public void Draw(PaintEventArgs e)
        {
            this.Shop.Draw(e);
        }

        /// <summary>
        /// Inits the simulation timer.
        /// </summary>
        private void InitTimer()
        {
            this.timer.Interval = GetUpdateInterval();
            this.timer.Tick += this.Timer_Tick;
            this.timer.Start();
        }

        /// <summary>
        /// On each timer ticker.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The EventArgs.</param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            if (this.customerTraffic.ElapsedMilliseconds > CreateCustomerInterval / SimulationSpeed)
            {
                this.customerTraffic.Restart();
                this.CreateCustomer();
            }

            this.Shop.Update();
        }

        /// <summary>
        /// Creates a new customer.
        /// </summary>
        private void CreateCustomer()
        {
            if (!this.Shop.IsFull())
            {
                var customer = new Customer(RandomNumber.Generate(18, 100 + 1));
                customer.EnterShop(this.Shop);
            }
        }
    }
}
